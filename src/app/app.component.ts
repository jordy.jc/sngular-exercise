import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'ejercicio';
  checkoutForm;
  sendNumber: any;
  nNumber: any;
  public showChild: boolean = false;
  disableButton = false;

  constructor(
    // private cartService: CartService,
    private formBuilder: FormBuilder
  ) {
    this.checkoutForm = this.formBuilder.group({
      number: '',
    });
  }

  onlyNumbers(event: any) {
    if (event.keyCode >= 48 && event.keyCode <= 57) {
      return true;
    }
    return false;
  }

  onSubmit(customerData: any) {
    console.log(customerData);
    // Process checkout data here
    //this.nNumber = this.cartService.clearCart();
    this.checkoutForm.reset();
    this.disableButton = false;
    this.sendNumber = customerData;
    this.showChild = true;

    //console.warn('Your order has been submitted', customerData);
  }

  onChange(event: any) {
    const newValue = (event.target as HTMLInputElement).value;
    newValue.length > 0
      ? (this.disableButton = true)
      : (this.disableButton = false);
  }

  closeModal(event: any) {
    console.log(event);
    this.showChild = event;
  }
}
