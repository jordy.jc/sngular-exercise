import { ModalResultsComponent } from './modal-results/modal-results.component';
import { ReactiveFormsModule, FormBuilder, FormsModule } from '@angular/forms';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [AppComponent, ModalResultsComponent],
      providers: [{ provide: FormBuilder, useValue: formBuilder }],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'ejercicio'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('ejercicio');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content > h2')?.textContent).toContain(
      'Calcula una serie'
    );
  });

  // it('should fill input and submit', fakeAsync(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   // const formCheck = component.checkoutForm;
  //   const el = fixture.debugElement.query(By.css('.formNumber'));
  //   const fnc = spyOn(component, 'onSubmit');

  //   const btnSubmit = fixture.debugElement.query(By.css('.card'));

  //   // spyOn(btnSubmit, "")

  //   fixture.detectChanges();
  //   const divDescription = fixture.debugElement.query(By.css('#number'));
  //   divDescription.nativeElement.value = '12';
  //   divDescription.nativeElement.dispatchEvent(new Event('input'));
  //   fixture.detectChanges();
  //   expect(divDescription.nativeElement.value).toContain('12');

  //   (btnSubmit.nativeElement as HTMLButtonElement).click();
  //   fixture.detectChanges();
  //   component.onSubmit('12');

  //   //el.triggerEventHandler("onSubmit", null)
  //   expect(fnc).toHaveBeenCalled()
  // }));

  it('should call save() method on form submit', () => {
    /*Get button from html*/
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    // Supply id of your form below formID
    const getForm = fixture.debugElement.query(By.css('.formNumber'));
    expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();
  });
});
