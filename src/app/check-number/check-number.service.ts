import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CheckNumberService {
  result: any;
  constructor() {}

  isPrimeNumber(val: any) {
    const isPrime = (num: any) => {
      for (let i = 2; i < num; i++) if (num % i === 0) return false;
      return num > 1;
    };

    this.result = isPrime(parseInt(val, 10)) ? true : false;
    return this.result;
  }

  isFibonacciNumber(val: any) {
    if (val == 0 || val == 1) {
      return true;
    }

    let a = 0,
      b = 1,
      c;

    while (true) {
      c = a + b;
      a = b;
      b = c;

      if (c == val) {
        return true;
      } else if (c >= val) {
        return false;
      }
    }
  }

  isTriangularNumber(val: any) {
    // Base case
    if (val <= 0) return false;

    // A Triangular number must be sum of first n
    // natural numbers
    let sum = 0;
    for (let n = 1; sum <= val; n++) {
      sum = sum + n;
      if (sum == val) return true;
    }

    return false;
  }

  calculateSeries(nNumber: any) {
    console.log((2 * nNumber * nNumber) / (nNumber - 2));
    return (2 * nNumber * nNumber) / (nNumber - 2);
  }
}
