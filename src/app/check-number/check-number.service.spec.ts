import { TestBed } from '@angular/core/testing';

import { CheckNumberService } from './check-number.service';

describe('CheckNumberService', () => {
  let service: CheckNumberService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CheckNumberService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
