import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { CheckNumberService } from '../check-number/check-number.service';

@Component({
  selector: 'app-modal-results',
  templateUrl: './modal-results.component.html',
  styleUrls: ['./modal-results.component.css'],
})
export class ModalResultsComponent implements OnInit {
  @Input() nNumber: any;
  @Input() removeHide: any;
  @Output() closeModal = new EventEmitter<boolean>();
  isPrimeNumber: any;
  isFibonacciNumber: any;
  isTriangularNumber: any;
  validateNumbers: any;
  res: any;

  constructor(private checkNumberService: CheckNumberService) {}

  ngOnInit(): void {
    console.log(this.removeHide);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['nNumber'].currentValue !== undefined) {
      //console.log(changes['nNumber'].currentValue['number']);

      this.nNumber = changes['nNumber'].currentValue['number'];

      this.isPrimeNumber = this.checkNumberService.isPrimeNumber(this.nNumber);
      this.isFibonacciNumber = this.checkNumberService.isFibonacciNumber(
        this.nNumber - 2
      );
      this.isTriangularNumber = this.checkNumberService.isTriangularNumber(
        this.nNumber
      );

      console.log({
        prime: this.isPrimeNumber,
        fibonacci: this.isFibonacciNumber,
        triangular: this.isTriangularNumber,
      });

      //this.validateNumbers();
      this.validateNumbers =
        this.isFibonacciNumber && this.isPrimeNumber && this.isTriangularNumber;
      console.log(this.validateNumbers);

      if (this.validateNumbers) {
        this.res = this.checkNumberService.calculateSeries(this.nNumber);
      }
    }
  }

  ngAfterViewInit() {}

  ngAfterViewChecked() {
    if (this.removeHide) {
      console.log('ABRIR');
      document.querySelector('.modal')?.classList.remove('hidden');
      document.querySelector('.overlay')?.classList.remove('hidden');
    }
  }

  onClose() {
    document.querySelector('.modal')?.classList.add('hidden');
    document.querySelector('.overlay')?.classList.add('hidden');

    console.log(98378);

    this.removeHide = false;
    this.closeModal.emit(false);
  }
}
